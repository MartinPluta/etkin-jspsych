var storeToFileSystem = true;
var storeToDatabase = false;

var timeline = [];
var practiceTries = 0;

var practiceTriesLimit = 3;
var requiredPracticePercentage = 65;
var termAngry = 'angry';
var termHappy = 'happy';
var termNone = 'none';
var keyCodeAngry = 39;
var keyCodeHappy = 37;
var keyCodeInstructionsContinue = 32; 
var stimuliPathPractice = 'images/stimuli/practice';
var stimuliPathTask = 'images/stimuli/task';
var stimuliTiming = 1000;
var fixationImage = 'images/fixation.png';
var pauseTimingPreSound = 40000;
var pauseTimingPostSound = 5000;
var pauseImage = 'images/pause.png';
var alarmSoundFile = 'audio/alarm.wav';
var termCorrect = "correct";
var termFalse ="false";
var termTimeout = "timeout";
var termPractice = "practice";
var termTask = "task";

var stimuliPractice = [
  { fileName: '30mac.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '31mhi.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '30mai.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '20fhc.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '30mhc.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '31mai.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '20fac.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '31mhc.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '20fai.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '30mhi.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '20fhi.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '31mac.jpg', solution: termAngry, fixationTiming: 1500 },
];

var stimuliTaskBlock1 = [
  { fileName: '1mhc.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '6fhi.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '11fac.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '3mac.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '2mhi.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '4mai.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '7fhi.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '4mhc.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '5mhc.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '1mai.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '5mac.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '6fai.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '4mhi.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '9fhc.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '10fhc.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '2mac.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '8fai.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '5mhi.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '3mhi.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '1mac.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '5mai.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '9fai.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '6fhc.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '2mac.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '3mhc.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '7fhi.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '5mai.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '2mhc.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '8fhi.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '1mac.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '5mac.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '9fhi.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '10fhi.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '2mai.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '7fac.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '6fhc.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '4mhc.jpg', solution: termHappy, fixationTiming: 1500 }
];

var stimuliTaskBlock2 = [
  { fileName: '1mai.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '11fhc.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '5mai.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '6fai.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '8fhc.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '1mac.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '4mac.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '11fhi.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '1mhi.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '3mac.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '6fai.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '9fac.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '7fhc.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '2mhi.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '3mai.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '1mai.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '11fhc.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '5mhc.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '1mac.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '5mai.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '2mac.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '2mac.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '3mhi.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '4mai.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '7fhi.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '4mhc.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '9fhc.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '11fai.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '1mhc.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '5mhi.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '3mai.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '10fac.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '6fhc.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '11fac.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '2mai.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '8fhi.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '9fhi.jpg', solution: termHappy, fixationTiming: 1500 }
];

var stimuliTaskBlock3 = [
  { fileName: '1mhc.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '6fhi.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '11fac.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '9fhc.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '3mhi.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '4mai.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '7fhi.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '10fhc.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '8fhc.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '1mai.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '5mac.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '6fai.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '4mhi.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '9fac.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '10fhc.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '2mac.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '8fai.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '5mhi.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '11fhi.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '1mhc.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '5mai.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '9fai.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '6fhc.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '10fac.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '3mhc.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '7fhi.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '11fai.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '2mhc.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '8fhi.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '1mac.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '5mac.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '9fai.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '10fhi.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '2mai.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '3mac.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '6fac.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '11fhc.jpg', solution: termHappy, fixationTiming: 1500 }
];

var stimuliTaskBlock4 = [
  { fileName: '3mai.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '4mhc.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '5mai.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '6fai.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '8fhc.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '7fac.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '4mac.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '11fhi.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '1mhi.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '3mhc.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '6fhi.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '9fac.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '7fhc.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '2mhi.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '4mai.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '8fai.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '11fac.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '10fhc.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '1mac.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '5mai.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '9fac.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '6fac.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '3mhi.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '8fai.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '7fhi.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '2mhc.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '9fhc.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '11fai.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '1mhc.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '5mhi.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '3mai.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '10fac.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '1mhc.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '11fac.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '2mai.jpg', solution: termAngry, fixationTiming: 1500 },
  { fileName: '8fhi.jpg', solution: termHappy, fixationTiming: 1500 },
  { fileName: '9fhi.jpg', solution: termHappy, fixationTiming: 1500 }
];

var allStimuli = stimuliPractice.concat(stimuliTaskBlock1).concat(stimuliTaskBlock2).concat(stimuliTaskBlock3).concat(stimuliTaskBlock4);

var instructionsPractice = '<h1>Instruktionen (Training)</h1><br/>\
                            In der folgenden Aufgabe werden Ihnen nacheinander verschiedene Gesichter präsentiert, die entweder Ärger oder Freude ausdrücken. Auf den Gesichtern wird zudem entweder das Wort „Ärger“ oder das Wort „Freude“ gedruckt sein.<br/><br/>\
                            Ihre Aufgabe wird sein zu entscheiden, ob das Gesicht Ärger oder Freude ausdrückt und dabei das auf das Bild gedruckte Wort zu ignorieren.<br/><br/>\
                            Wenn das Gesicht Ärger ausdrückt, drücken Sie bitte die rechte Pfeiltaste, wenn das Gesicht Freude ausdrückt, drücken Sie bitte die linke Pfeiltaste.<br/><br/>\
                            Bitte antworten Sie so schnell und so korrekt wie möglich.<br/><br/>\
                            Wir beginnen zunächst mit einigen Übungsdurchläufen.<br/><br/>\
                            Weiter mit Leertaste';

var instructionsTasks = '<h1>Instruktionen</h1><br/>\
                            In der folgenden Aufgabe werden Ihnen nacheinander verschiedene Gesichter präsentiert, die entweder Ärger oder Freude ausdrücken. Auf den Gesichtern wird zudem entweder das Wort „Ärger“ oder das Wort „Freude“ gedruckt sein.<br/><br/>\
                            Ihre Aufgabe wird sein zu entscheiden, ob das Gesicht Ärger oder Freude ausdrückt und dabei das auf das Bild gedruckte Wort zu ignorieren.<br/><br/>\
                            Wenn das Gesicht Ärger ausdrückt, drücken Sie bitte die rechte Pfeiltaste, wenn das Gesicht Freude ausdrückt, drücken Sie bitte die linke Pfeiltaste.<br/><br/>\
                            Bitte antworten Sie so schnell und so korrekt wie möglich.<br/><br/>\
                            Wir beginnen zunächst mit einigen Übungsdurchläufen.<br/><br/>\
                            Weiter mit Leertaste';
                            