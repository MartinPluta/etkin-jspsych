function createStimuliTimeline(stimuli, path, instructions) {
  instructions = instructions || 0;
  var stimuliTimeline = [];
    if (instructions) {
      stimuliTimeline.push(instructions);
    }
    for (var i in stimuli) {
      var fixation = { stimulus: fixationImage, timing_response: stimuli[i].fixationTiming, response_ends_trial: false };  
      stimuliTimeline.push(fixation);
      var stimulus = { stimulus: path + '/' + stimuli[i].fileName, timing_response: stimuliTiming, choices: [keyCodeAngry, keyCodeHappy] };
      stimuliTimeline.push(stimulus);
    }
  return stimuliTimeline;
}

function isPracticePassed(data) {
  practiceTries++;
  var percentage = getPracticePercentage(data);
  if ((percentage >= requiredPracticePercentage) || (practiceTries >= practiceTriesLimit)) {
    return true;
  } else {
    return false;
  }
}

function getPracticePercentage(data) {
  var correctAnswers = 0;
  var totalAnswers = 0;
  var stimulusNumber = 0;
    for (var i in data) {
      if (isStimulus(data[i])) {
        totalAnswers++;
        var decodedResponse = decodeStimulusInput(data[i]);
        if (decodedResponse == stimuliPractice[stimulusNumber].solution) {
          correctAnswers++;
        }
        stimulusNumber++;
      }
    }
  return calculatePercentage(correctAnswers, totalAnswers);
}

function calculatePercentage(fraction, total) {
  return (fraction / total) * 100;
}

function createFormattedData(data) {
  var formattedData = {};
  var correct = 0;
  var incorrect = 0;
  var timeout = 0;
  formattedData.subject = {};
  formattedData.session = {};
  formattedData.stimuli = [];
    for (var i in data) {
      data[i] = deleteUnusedData(data[i]);
        if (hasResponse(data[i])) {
          var responses = JSON.parse(data[i].responses);
          formattedData.subject = responses.Q0;
          formattedData.session = responses.Q1;
        }
        if (isStimulus(data[i])) {
          data[i].type = decodeType(data[i]);
          data[i].response = decodeStimulusInput(data[i]);
          data[i].expected = decodeExpectedResponse(data[i], allStimuli);
          data[i].quality = decodeResponseQuality(data[i], allStimuli);
          if (decodeCorrectResponse(data[i])) {
            correct++;
          }
          if (decodeIncorrectResponse(data[i])) {
            incorrect++;
          }
          if (decodeTimeout(data[i])) {
            timeout++;
          }
          data[i].correct = correct;
          data[i].incorrect = incorrect;
          data[i].timeout = timeout;
          data[i].correctPercentage = decodeCorrectPercentage(correct, incorrect, timeout);
          data[i].incorrectPercentage = decodeIncorrectPercentage(correct, incorrect, timeout);
          data[i].timeoutPercentage = decodeTimeoutPercentage(correct, incorrect, timeout);
          data[i].quality = decodeResponseQuality(data[i], allStimuli);
          formattedData.stimuli.push(data[i]);
        }
   }
  return formattedData;
}

function isStimulus(item) {
  if (typeof item.stimulus != "undefined") {
    if (item.stimulus.indexOf('stimuli') !== -1) {
      return true;
    } else {
      return false;
    }
  }
}

function hasResponse(item) {
  if (typeof item.responses != "undefined") {
    return true;
  } else {
    return false;
  }
}

function decodeCorrectResponse(item) {
  if (item.quality == termCorrect) {
    return true;
  }
}

function decodeIncorrectResponse(item) {
  if (item.quality == termFalse) {
    return true;
  }
}

function decodeTimeout(item) {
  if (item.quality == termTimeout) {
    return true;
  }
}

function decodeCorrectPercentage(correct, incorrect, timeout) {
  var numberOfItems = correct + incorrect + timeout;
  var percent = numberOfItems / 100;
  return correct / percent;
}

function decodeIncorrectPercentage(correct, incorrect, timeout) {
  var numberOfItems = correct + incorrect + timeout;
  var percent = numberOfItems / 100;
  return incorrect / percent;
}

function decodeTimeoutPercentage(correct, incorrect, timeout) {
  var numberOfItems = correct + incorrect + timeout;
  var percent = numberOfItems / 100;
  return timeout / percent;
}

function decodeType(item) {
  if (item.stimulus.indexOf("practice") !== -1) {
    return termPractice;
  } else if (item.stimulus.indexOf("task") !== -1) {
    return termTask;
  }
}

function decodeStimulusInput(item) {
  if (item.key_press == keyCodeAngry) {
    return termAngry;
  } else if (item.key_press == keyCodeHappy) {
    return termHappy;
  } else {
    return termNone;
  }
}

function decodeExpectedResponse(item, stimuli) {
  var expected = "";
  stimuli.forEach(function(stimulus) {
    if (item.stimulus.indexOf("/" + stimulus.fileName) !== -1) {
      expected = stimulus.solution;
    }
  });
  return expected;
}

function decodeResponseQuality(item, stimuli) {
  var quality = "";
  if (item.response == termNone) {
    quality = termTimeout;
  } else {
    stimuli.forEach(function(stimulus) {
      if (item.stimulus.indexOf("/" + stimulus.fileName) !== -1) {
        if (item.response == stimulus.solution) {
          quality =  termCorrect;
        } else {
          quality =  termFalse;
        }
      }
    });
  }
  return quality; 
}

function deleteUnusedData(item) {
  delete item.time_elapsed;
  delete item.internal_node_id;
  delete item.trial_index;
  delete item.trial_type;
  return item;
}

function writeResultsToFileSystem(result) {
  var data = JSON.stringify(result);
  $.ajax({
    url: 'api/v1/results/filesystem',
    type: 'POST',
    data: result,
    dataType: 'json',
    success: function (data) {
      console.log(data);
    },
    error: function (jqXHR, tranStatus, errorThrown) {
      alert('Status: ' + jqXHR.status + ' ' + jqXHR.statusText + '. ' +
      'Response: ' + jqXHR.responseText);
    }
  });
}

function insertResultsToDatabase(result) {
  var data = JSON.stringify(result);
  $.ajax({
    url: 'api/v1/results/database',
    type: 'POST',
    data: result,
    dataType: 'json',
    success: function (data) {
      console.log(data);
    },
    error: function (jqXHR, tranStatus, errorThrown) {
      alert('Status: ' + jqXHR.status + ' ' + jqXHR.statusText + '. ' +
      'Response: ' + jqXHR.responseText);
    }
  });
}