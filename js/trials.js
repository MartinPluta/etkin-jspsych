var requestCredentials = {
  type: 'survey-text',
  questions: ["Please enter your <u>subject</u> number:", "Please enter your <u>session</u> number:"],
};

var instructionsPractice = {
  type: 'text',
  text: instructionsPractice,
  cont_key: [keyCodeInstructionsContinue]
}

var practice = {
  type: 'single-stim',
  timeline: createStimuliTimeline(stimuliPractice, stimuliPathPractice, instructionsPractice),
  timing_post_trial: 0,
  loop_function: function(data) {
    if (isPracticePassed(data)) {
      return false;
    } else {
      var percentage = getPracticePercentage(data)
      var message = percentage.toFixed(0) + "% richtige Antworten (" + requiredPracticePercentage + "% erforderlich)\n\n" + practiceTries + " von " + practiceTriesLimit + " Versuchen";
      alert(message);
      return true;
    }
  }
}

var instructionsTask = {
  type: 'text',
  text: instructionsTasks,
  cont_key: [keyCodeInstructionsContinue]
}

var stimuliTaskBlock1 = {
  type: 'single-stim',
  timeline: createStimuliTimeline(stimuliTaskBlock1, stimuliPathTask),
  timing_post_trial: 0
}

var stimuliTaskBlock2 = {
  type: 'single-stim',
  timeline: createStimuliTimeline(stimuliTaskBlock2, stimuliPathTask),
  timing_post_trial: 0
}

var stimuliTaskBlock3 = {
  type: 'single-stim',
  timeline: createStimuliTimeline(stimuliTaskBlock3, stimuliPathTask),
  timing_post_trial: 0
}

var stimuliTaskBlock4 = {
  type: 'single-stim',
  timeline: createStimuliTimeline(stimuliTaskBlock4, stimuliPathTask),
  timing_post_trial: 0
}

var blockBreakPreSound = {
  type: 'single-stim',
  stimulus: pauseImage,
  timing_response: pauseTimingPreSound,
  response_ends_trial: false,
  on_finish: function(data) {
    var audio = new Audio(alarmSoundFile);
    audio.play();
  }
}

var blockBreakPostSound = {
  type: 'single-stim',
  stimulus: pauseImage,
  timing_response: pauseTimingPostSound,
  response_ends_trial: false
}
