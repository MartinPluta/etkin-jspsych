<?php

$app->get('/results', function ($request, $response, $args) {
  $sth = $this->db->prepare("SELECT * FROM results ORDER BY id");
  $sth->execute();
  $scores = $sth->fetchAll();
  return $this->response->withJson($scores);
});

$app->post('/results/database', function ($request, $response) {
  $data = $request->getParsedBody();
  insertResultsToDatabase($data);
  $input['id'] = $this->db->lastInsertId();
  return $this->response->withJson($data);
});

$app->post('/results/filesystem', function ($request, $response) {
  $data = $request->getParsedBody();
  writeToCSV($data);
  return $this->response->withJson($data);
});

function insertResultsToDatabase($data) {
  $sql = "INSERT INTO results (subject, session, data) VALUES (:subject, :session, :data)";
  $sth = $this->db->prepare($sql);
  $sth->bindParam("subject", $data["subject"]);
  $sth->bindParam("session", $data["session"]);
  $sth->bindParam("data", json_encode($data["stimuli"]));
  $sth->execute();
}

function writeToCSV($data) {
  $csv = "sep=,\n";
  $csv .= "Type,Stimulus,ResponseTime,KeyPress,Response,Expected,Quality,Correct (Overall Count),False (Overall Count),Timeout (Overall Count),Correct (Overall Percentage),False (Overall Percentage),Timeout (Overall Percentage)\n";
    foreach ($data["stimuli"] as $stimulus){
      $csv.= $stimulus["type"] . ',' . $stimulus["stimulus"] . ',' . $stimulus["rt"] . ',' .  $stimulus["key_press"] . ',' . $stimulus["response"] . ',' . $stimulus["expected"] . ',' . $stimulus["quality"] . ',' . $stimulus["correct"] . ',' . $stimulus["incorrect"] . ',' . $stimulus["timeout"] . ',' . $stimulus["correctPercentage"] . ',' . $stimulus["incorrectPercentage"] . ',' . $stimulus["timeoutPercentage"] . "\n";
    }
  $date = date('Y-m-d-h-i-s', time());
  $path = '../../results';
    if (!file_exists($path)) {
      mkdir($path, 0777, true);
    }
  $filename = $path . '/' . $date . '-subject-' . $data["subject"] . '-session-' . $data["session"] . '.csv';
  $csv_handler = fopen ($filename, 'w');
  fwrite ($csv_handler, $csv);
  fclose ($csv_handler);
} 