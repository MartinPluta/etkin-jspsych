### Installation ###

* XAMPP, GIT und Composer sollten auf dem System eingerichtet werde
* XAMPP php.ini öffnen und max_input_vars = 10000 setzen. (Kommentar entfernen, um die Zeile zu aktivieren)
* Deie Eingabeaufforderung aufrufen (Windows-Taste + R und dann "cmd" als Befehl eingeben und ausführen)
* Dieses Repository von der Eingabeaufforderung in den "htdocs" Ordner im "xampp" Verzeichnis mit GIT clonen
* In das Anwendungsverzeichnis wechseln und im Unterordner "api" den Befehl "composer install" ausführen
* XAMPP starten
* Anwendung im browser aufrufen ("localhost/etkin-jspsych" als url eingeben)